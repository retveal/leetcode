import java.util.Arrays;

public class Solution {
    public static boolean isPalindrome(int x) {

        char[] array = String.valueOf(x).toCharArray();

        char[] reverseArray = String.valueOf(x).toCharArray();

        for (int i = 0; i < reverseArray.length / 2; i++) {
            char temp = reverseArray[i];
            reverseArray[i] = reverseArray[reverseArray.length - i - 1];
            reverseArray[reverseArray.length - i - 1] = temp;
        }

        if (Arrays.equals(array, reverseArray)) {
            return true;
        }
        return false;
    }
}
