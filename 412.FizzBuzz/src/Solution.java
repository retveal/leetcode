import java.util.ArrayList;
import java.util.List;

public class Solution {
    public List<String> fizzBuzz(int n) {
        
        List<String> array = new ArrayList<>();

        for (int i = 0; i < n; i++) {
            if ((i + 1) % 3 == 0) {
                if ((i + 1) % 5 == 0) {
                    array.add(i, "FizzBuzz");
                } else {
                    array.add(i, "Fizz");
                }
            } else if ((i + 1) % 5 == 0) {
                array.add(i, "Buzz");
            } else {
                array.add(Integer.toString(i + 1));
            }
        }
        return array;
    }
}
