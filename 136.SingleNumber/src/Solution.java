import java.util.Arrays;

public class Solution {

    public int singleNumber(int[] nums) {
        Arrays.sort(nums);

        for (int i = 0; i < nums.length - 1; i ++) {
            if (nums[i + 1] == nums[i]) {
                i++;
            } else {
                return nums[i];
            }
        }
        return nums[nums.length - 1];
    }
}
