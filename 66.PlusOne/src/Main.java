import java.util.Arrays;

public class Main {

    public static void main(String[] args) {
        int[] digits = {9, 9};

        Solution solution = new Solution();

        System.out.println(Arrays.toString(solution.plusOne(digits)));
    }
}
