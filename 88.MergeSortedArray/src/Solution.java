import java.util.Arrays;

public class Solution {
    public void merge(int[] nums1, int m, int[] nums2, int n) {
        int i = nums1.length - 1;
        for (int j = nums2.length - 1; j >= 0; j--) {
            nums1[i] = nums2[j];
            i--;
        }
        Arrays.sort(nums1);
    }
}
