public class Solution {
    public boolean isSymmetric(TreeNode root) {
        if (root == null) {
            return true;
        }

        return isSymmetric(root.left, root.right);
    }

    public boolean isSymmetric(TreeNode rootLeft, TreeNode rootRight) {
        if (rootLeft == null && rootRight == null) {
            return true;
        } else if (rootLeft == null || rootRight == null) {
            return false;
        } else if (rootLeft.val != rootRight.val) {
            return false;
        }
        return isSymmetric(rootLeft.left, rootRight.right) && isSymmetric(rootRight.left, rootLeft.right);
    }
}
