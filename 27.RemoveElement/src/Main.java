public class Main {

    public static void main(String[] args) {
        Solution solution1 = new Solution();
        int[] nums = {3, 2, 2, 3};
        System.out.println(solution1.removeElement(nums, 3));

        Solution solution2 = new Solution();
        int[] nums2 = {0, 1, 2, 2, 3, 0, 4, 2};
        System.out.println(solution2.removeElement(nums2, 2));
    }
}
