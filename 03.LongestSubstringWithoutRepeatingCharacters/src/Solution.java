import java.util.HashMap;
import java.util.Map;

public class Solution {
    public int lengthOfLongestSubstring(String s) {
        char[] stringToArray = s.toCharArray();

        Map<Integer, Character> map = new HashMap<>();

        for (int i = 0; i < stringToArray.length; i++) {
            char current = stringToArray[i];
            map.put(i, current);
        }

        int count = 1;
        for (int i = 0; i < map.size(); i++) {
            if (map.get(i + 2) != null) {
                char current = map.get(i);
                char next = map.get(i + 1);
                char nextNext = map.get(i + 2);

                if (current != next && next != nextNext) {
                    count++;
                }
            }
        }
        return count;
    }
}
