public class Solution {
    public ListNode mergeTwoLists(ListNode list1, ListNode list2) {
        ListNode newList = new ListNode(0);
        ListNode temp = newList;

        while (list1 != null || list2 != null) {

            if ((list1 != null && list2 != null && list1.val < list2.val) || list2  == null) {

                temp.next = new ListNode(list1.val);
                list1 = list1.next;

            } else {

                temp.next = new ListNode(list2.val);
                list2 = list2.next;
            }

            temp = temp.next;
        }

        return newList.next;
    }
}
