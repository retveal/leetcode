public class Solution {
    
    public ListNode middleNode(ListNode head) {

        ListNode node = head;
        int num = 0;

        while (head != null) {
            head = head.next;
            num++;
        }

        for (int i = 0; i < num / 2; i++) {
            node = node.next;
        }

        return node;
    }
}
