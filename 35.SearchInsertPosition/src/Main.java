public class Main {

    public static void main(String[] args) {
        Solution solution = new Solution();
        int[] nums1 = {1, 3, 5, 6};

        int[] nums2 = {3, 6, 7, 8, 10};

        System.out.println(solution.searchInsert(nums1, 5));
        System.out.println(solution.searchInsert(nums1, 2));
        System.out.println(solution.searchInsert(nums1, 7));
        System.out.println(solution.searchInsert(nums1, 4));

        System.out.println(solution.searchInsert(nums2, 5));
    }
}
