public class Solution {
    public int searchInsert(int[] nums, int target) {

        for (int i = 0; i < nums.length; i++) {
            if (target > nums[nums.length - 1]) {
                return nums.length;
            }
            if (nums[i] != target) {
                if (target - 1 == nums[i]) {
                    return i + 1;
                }
                if (target + 1 == nums[i]) {
                    return i;
                }
            } else {
                return i;
            }
        }
        return 0;
    }
}
