import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class Solution {
    public int romanToInt(String s) {

        Map<Integer, String> map = new HashMap<>();
        Set<Map.Entry<Integer, String>> entrySet = map.entrySet();

        map.put(1, "I");
        map.put(5, "V");
        map.put(10, "X");
        map.put(50, "L");
        map.put(100, "C");
        map.put(500, "D");
        map.put(1000, "M");

        String[] array = s.split("");
        int sum = 0;
        int previous = 0;

        for (String value : array) {
            for (Map.Entry<Integer, String> entry : entrySet) {
                if (value.equals(entry.getValue())) {
                    if (previous < entry.getKey()) {
                        sum -= previous;
                        sum += entry.getKey() - previous;
                    } else {
                        sum += entry.getKey();
                    }

                    previous = entry.getKey();
                }
            }
        }
        return sum;
    }
}
