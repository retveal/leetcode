import java.util.Arrays;

public class Solution {

    public int[] intersect(int[] nums1, int[] nums2) {
        int[] array = new int[1001];
        int[] result = new int[1001];

        for (int i = 0; i < nums1.length; i++) {
            array[nums1[i]]++;
        }

        int count = 0;
        for (int i = 0; i < nums2.length; i++) {
            if (array[nums2[i]] > 0) {
                result[count++] = nums2[i];
                array[nums2[i]]--;
            }
        }
        return Arrays.copyOfRange(result, 0, count);
    }
}
