public class Solution {
    public boolean carPooling(int[][] trips, int capacity) {

        int farthest = 0;
        for (int i = 0; i < trips.length; i++) {
            farthest = Math.max(farthest, trips[i][2]);
        }

        int[] arr = new int[farthest + 1];
        for (int i = 0; i < trips.length; i++) {
            int value = trips[i][0], left = trips[i][1], right = trips[i][2];
            arr[left] += value;
            arr[right] -= value;
        }

        int curr = 0;
        for (int i = 0; i < arr.length; i++) {
            curr += arr[i];
            if (curr > capacity) {
                return false;
            }
        }

        return true;
    }
}
