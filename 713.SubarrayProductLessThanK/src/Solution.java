public class Solution {
    public int numSubarrayProductLessThanK(int[] nums, int k) {
        int result = 0;
        int left = 0;
        int current = 1;

        for (int right = 0; right < nums.length; right++) {
            current *= nums[right];
            while (left <= right && current >= k) {
                current /= nums[left];
                left++;
            }
            result += right - left + 1;
        }
        return result;
    }
}
