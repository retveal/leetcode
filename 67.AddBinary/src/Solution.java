
public class Solution {
    public String addBinary(String a, String b) {

        StringBuilder stringBuilder = new StringBuilder();

        int i = a.length() - 1;
        int j = b.length() - 1;
        int carry = 0;

        while (i >= 0 || j >= 0) {
            int result = carry;
            if (j >= 0) {
                result += b.charAt(j--) - '0';
            }
            if (i >= 0) {
                result += a.charAt(i--) - '0';
            }

            stringBuilder.append(result % 2);
            carry = result / 2;
        }

        if (carry != 0)  {
            stringBuilder.append(carry);
        }

        return stringBuilder.reverse().toString();
    }
}
