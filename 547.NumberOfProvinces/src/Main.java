public class Main {

    public static void main(String[] args) {
        int[][] isConnected = {{1, 1, 0}, {1, 1, 0}, {0, 0, 1}};
        int[][] isConnected1 = {{1, 0, 0}, {0, 1, 0}, {0, 0, 1}};
        Solution solution = new Solution();
        System.out.println(solution.findCircleNum(isConnected));
        System.out.println(solution.findCircleNum(isConnected1));
    }
}
