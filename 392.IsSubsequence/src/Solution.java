public class Solution {
    public boolean isSubsequence(String s, String t) {

        int[] array = new int[10000];

        if (t.length() < s.length()) {
            return false;
        }

        int i = 0;
        int j = 0;

        while (s.length() > i && t.length() > j) {
            if (s.charAt(i) == t.charAt(j)) {
                i++;
            }
            j++;
        }
        return i == s.length();
    }
}
